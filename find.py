import re


def find_package() -> None:
    """Выводит только те пакеты, у которых в конце строки присутствует 0"""
    with open("requirements.txt", "r") as requirements:
        packages = requirements.read()

    # Любые символы до (знака == и любых символов за которыми следует 0 и символ перевода строки)
    # (Конструкция Positive lookahead)
    packages_with_0_at_end_of_line = re.findall(
        pattern=r".+(?===.+\.0$)|.+(?===0$)",
        string=packages,
        flags=re.MULTILINE
    )

    # Можно найти все пакеты с версиями с помощью флага re.MULTILINE (re.M)
    # packages_with_0_at_end_of_line = re.findall(
    #     pattern=r".+?0$",
    #     string=packages,
    #     flags=re.MULTILINE
    # )

    print(packages_with_0_at_end_of_line)


if __name__ == '__main__':
    find_package()
